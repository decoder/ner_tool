FROM tiangolo/uwsgi-nginx-flask:python3.8


# --------------------
# Package installation
# --------------------

ARG DEBIAN_FRONTEND=noninteractive
ARG THREADS

RUN apt-get clean && apt-get update && apt-get install -y -qq \
        apt-utils \
        bzip2 \
        build-essential \
        curl \
        dialog \
        gawk \
        locales \
        openssl \
        python3 \
        python3-pip \
        wget

COPY requirements.txt /
RUN pip install --upgrade pip
RUN pip install -r /requirements.txt

# Should switch from limaserver to lima-python. The wheel import and install
# below are first step toward this
# COPY --from aymara/lima-manylinux_2_24 wheelhouse/aymara-0.3.0-cp38-cp38-manylinux_2_24_x86_64.whl /aymara-0.3.0-cp38-cp38-manylinux_2_24_x86_64.whl
# RUN pip install /aymara-0.3.0-cp38-cp38-manylinux_2_24_x86_64.whl

COPY ./api /app
COPY ./ssl /ssl

ARG PUID
ARG GUID

RUN echo "gid = ${GUID}" >> /app/uwsgi.ini
RUN echo "uid = ${PUID}" >> /app/uwsgi.ini

# CREATE APP USER
RUN groupadd -g ${GUID} apiuser && \
    useradd -r -u ${PUID} -g apiuser apiuser

RUN chown -R ${PUID}:${GUID} /app

# install SSL certificate of PKM server
RUN mkdir -p /usr/local/share/ca-certificates && chmod 755 /usr/local/share/ca-certificates && cp -f /ssl/pkm_docker.crt /usr/local/share/ca-certificates && chmod 644 /usr/local/share/ca-certificates/pkm_docker.crt && update-ca-certificates

