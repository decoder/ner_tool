#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/pkm_rest_api.sh"

host="localhost"
port=702

# For the BankingExample.java example
PROJECT_NAME="mythdb"
PKM_USER=mythuser
PKM_PASSWORD=hello


function encode
{
  result=$(python -c "import urllib.parse;print(urllib.parse.quote('$1'))")
  echo $result
}

# For the BankingExample.java example
#  [{"comments": [{"loc": {"pos_start": {"pos_cnum": 250, "pos_lnum": 9}, "pos_end": {"pos_cnum": 276, "pos_lnum": 9}}, "comments": ["Constructor of the class"]}, {"loc": {"pos_start": {"pos_cnum": 392, "pos_lnum": 16}, "pos_end": {"pos_cnum": 428, "pos_lnum": 16}}, "comments": ["Increments a credit in the balance"]}, {"loc": {"pos_start": {"pos_cnum": 639, "pos_lnum": 24}, "pos_end": {"pos_cnum": 674, "pos_lnum": 24}}, "comments": ["Decrements a debit in the balance"]}, {"loc": {"pos_start": {"pos_cnum": 871, "pos_lnum": 33}, "pos_end": {"pos_cnum": 911, "pos_lnum": 33}}, "comments": ["locks the account to prevent movements"]}, {"loc": {"pos_start": {"pos_cnum": 1008, "pos_lnum": 39}, "pos_end": {"pos_cnum": 1047, "pos_lnum": 39}}, "comments": ["Retrieves the current account Balance"]}], "fileEncoding": "utf-8", "fileMimeType": "application/json", "sourceFile": "BankingExample.java", "fileFormat": "json", "type": "Comment"}]
PKM_PATH=$(encode "code/java/comments/${PROJECT_NAME}" )

# For the BankingExample.java example
ACCESS=$(encode '[0].comments[*].comments[0]')

USER1_KEY=$(login ${PKM_USER} ${PKM_PASSWORD})
echo "Login was successful"

command="curl --request POST  --url http://${host}:${port}/pkmner --header 'content-type: application/json' --header 'accept: application/json'  --header 'key: ${USER1_KEY}' --data '{\"project_id\": \"${PROJECT_NAME}\", \"path\": \"${PKM_PATH}\", \"access\": \"${ACCESS}\"}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
