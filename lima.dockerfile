FROM aymara/lima-manylinux_2_24:latest

EXPOSE 8080/tcp

# --------------------
# Package installation
# --------------------

ARG DEBIAN_FRONTEND=noninteractive
ARG THREADS

ARG LIMAPUID=2001
ARG LIMAGUID=2001

COPY ./lima-PackRun /PackRun

ENV LIMA_CONF /PackRun/config:/usr/share/config/lima
ENV LIMA_RESOURCES /PackRun/resources:/usr/share/apps/lima/resources

# CREATE APP USER
RUN groupadd -g ${LIMAGUID} limauser && \
    useradd -r -u ${LIMAPUID} -g limauser limauser

USER limauser
ENTRYPOINT ["limaserver", "-l", "eng"]
