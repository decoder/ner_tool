apispec
apispec[validation]
apispec-webframeworks
ca-bundle
flask
jmespath==0.10.0
marshmallow
pyyaml
requests
pkm-python-client==0.1.22

