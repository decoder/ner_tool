# Decoder NER tool

This is the Named Entities Recognition tool for the Decoder project (WP4)

It is a Docker composition of Flask service and a LIMA service as backend.

It is currently a working service with initial I/O formats.

Call `build.sh` to build the Docker container. Then `run_docker.sh` to start it
and finally `test_ner.sh` to test the service. To test its interaction with the PKM, use `test_ner_pkm.sh` and `test_ner_java_pkm.sh`.


## NER API

The NER tool, when interacting with the pkm, takes as parameters the information necessary to find in the pkm a text segment. Its return value is only a pair of a string and a code indicating the success of the operation or explaining its failure (it will be later completed with the id of the artifact added to the PKM annotations collection). The actual result is an annotation added to the Annotations collection of the pkm.

`access` is expressed using the [JMESPath](https://jmespath.org/) query language.

### Input:
  * project_id: pkm project to query
  * path: pkm path to query to retrieve json data containing the text
  * access: json path allowing to retrieve the text to analyze in the retrieved data

### Output:
  * status: boolean indicating the success or failure of the call
  * message: "Success" if status is true and an error message otherwise
  * (not implemented for now) artifactId: the ID of the artifact added to the PKM annotations collection

### Side effect:

If successful, an artefact is added to the pkm in the array at the path: `/annotations/{project_id}`. The artefact content is a JSON structure of the form:

```
{
  "path": "<string>",
  "access": "<string>",
  "ner":
  [
    {
      "entities":
      [
        {"text": <string>, "type": <string>, "pos": <int>, "len": <int>},
        …
      ]
    },
    …
  ]
}
```

## Example

A call with a C function comment (parameters and result values are in reality encoded to make valid json strings, even when it is not explicitly stated).

The comment is *"Sum of two vectors\n"*.

Query:

```
{
  "project_id": "mydb",
  "path": "code/c/comments/mydb",
  "access": "[?global_kind == \'GFun\'] | [?loc.pos_start.pos_path == \'examples/vector2.c\'].comments | [0]"
}
```

The result with this call is writen in the pkm at `/annotations/mydb` Its value is:
```
{
  'path': 'code/c/comments/mydb',
  'access': '[?global_kind == 'GFun'] | [?loc.pos_start.pos_path == 'examples/vector2.c'].comments | [0]',
  'ner': [
    {
      'entities': [
        {'text': 'Sum', 'type': 'FunctionName', 'pos': 1, 'len': 4},
        {'text': 'two', 'type': 'NUMBER', 'pos': 8, 'len': 11},
        {'text': 'n', 'type': 'Identifier', 'pos': 20, 'len': 21}
      ]
    }
  ]
}
```

### OpenAPI api

The OpenAPI api of this service can be retrieved by pointing a navigator to the
URL of the service. As of 14/01/2021, it is:

```json
{
  "components": {
    "schemas": {
      "NERChunk": {
        "properties": {
          "len": {
            "type": "integer"
          },
          "pos": {
            "type": "integer"
          },
          "text": {
            "type": "string"
          },
          "type": {
            "type": "string"
          }
        },
        "required": [
          "len",
          "pos",
          "text",
          "type"
        ],
        "type": "object"
      },
      "NERResult": {
        "properties": {
          "entities": {
            "items": {
              "$ref": "#/components/schemas/NERChunk"
            },
            "type": "array"
          }
        },
        "required": [
          "entities"
        ],
        "type": "object"
      },
      "PKMNERResult": {
        "properties": {
          "message": {
            "type": "string"
          },
          "status": {
            "type": "boolean"
          }
        },
        "required": [
          "message",
          "status"
        ],
        "type": "object"
      }
    }
  },
  "info": {
    "description": "A Named Entities Recognition tool for the Decoder project",
    "title": "Decoder NER tool",
    "version": "0.0.1"
  },
  "openapi": "3.0.2",
  "paths": {
    "/ner": {
      "post": {
        "parameters": [
          {
            "in": "data",
            "name": "text",
            "required": false,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "schema": {
              "$ref": "#/components/schemas/NERResult"
            }
          }
        }
      }
    },
    "/pkmner": {
      "post": {
        "parameters": [
          {
            "description": "Access key to the PKM (optional, depending on server configuration)",
            "in": "header",
            "name": "keyParam",
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "access",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "path",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "model",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "project_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PKMNERResult"
                }
              }
            },
            "description": "NER completed."
          },
          "500": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PKMNERResult"
                }
              }
            },
            "description": "An unexpected error has occurred."
          }
        }
      }
    }
  }
}
```

