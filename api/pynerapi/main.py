#!/usr/bin/env python3
"""
Copyright 2021 CEA LIST

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import os
from logging.config import dictConfig
import json
from flask import Flask
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields
import ca_bundle

ca_bundle.install()

from .v1.routes import pkmner as pkmner
from .v1.routes import ner as ner

spec = APISpec(
    title="Decoder NER tool",
    version="0.0.1",
    openapi_version="3.0.2",
    info=dict(description="A Named Entities Recognition tool for the Decoder project"),
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)


def _initialize_errorhandlers(application):
    '''
    Initialize error handlers
    '''
    from .errors import errors
    application.register_blueprint(errors)


def create_app():

    dictConfig({
        'version': 1,
        'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }},
        'handlers': {'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }},
        'root': {
            'level': 'DEBUG',
            'handlers': ['wsgi']
        }
    })

    # Create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config['SECRET_KEY'] = 'b0bff1b3-32d1-4ec5-b6e6-f31af9ed682e'

    with app.app_context():
        #ensure_dir(app.instance_path)  # Ensure the instance folder exists

        ## Create temp directory where text files will be stored
        #app.config.txt_folder = os.path.join(app.instance_path, "temp_text")
        #ensure_dir(app.config.txt_folder)

        # Register home blueprint
        from . import home
        app.register_blueprint(home.bp)
        _initialize_errorhandlers(app)

        # Register api v1
        from .v1.routes import api as api_v1
        app.register_blueprint(api_v1, url_prefix="/")

        return app


app = create_app()

# we need to be in a Flask request context
with app.test_request_context():
    spec.path(view=ner)
    spec.path(view=pkmner)
