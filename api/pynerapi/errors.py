"""
Copyright 2021 CEA LIST

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from flask import Blueprint, jsonify

errors = Blueprint('errors', __name__)

@errors.app_errorhandler(400)
def handle_bad_request_error(error):
    status_code = 400
    success = False
    response = {
        'success': success,
        'message': "Bad Request."
    }

    return jsonify(response), status_code


@errors.app_errorhandler(404)
def handle_not_found_error(error):
    status_code = 404
    success = False
    response = {
        'success': success,
        'message': "Not found."
    }

    return jsonify(response), status_code

@errors.app_errorhandler(405)
def handle_not_allowed_error(error):
    status_code = 405
    success = False
    response = {
        'success': success,
        'message': "The method is not allowed."
    }

    return jsonify(response), status_code

@errors.app_errorhandler(500)
def handle_internal_server_error(error):
    status_code = 500
    success = False
    response = {
        'success': success,
        'message': "Internal Server Error."
    }

    return jsonify(response), status_code


@errors.app_errorhandler(501)
def handle_not_implemented_error(error):
    status_code = 501
    success = False
    response = {
        'success': success,
        'message': 'Not Implemented Error. This fake service can only analyze a limited set of 1 sentence: "whichptr indicates which xbuffer holds the final iMCU row.".'
    }

    return jsonify(response), status_code


#@errors.app_errorhandler(Exception)
#def handle_unexpected_error(error):
    #status_code = 500
    #success = False
    #response = {
        #'success': success,
        #'message': 'An unexpected error has occurred.'
    #}

    #return jsonify(response), status_code

