$( document ).ready(function() {
    console.log( "ready!" );
    $( "#clearInputField" ).click(function() {
        $( "#raw_text" ).val("");
    });



    $('#city').change(function() {
        if (this.checked) {
            $( ".person.entity" ).css("background", "#bfeeb7");
            $( ".person.entity" ).css("margin", "0 0.25em");
            $( ".person.entity" ).css("padding", "0.45em 0.6em");
            $( ".person.entity span.category" ).css("display", "inline");
        } else {
            $( ".person.entity" ).css("background", "none");
            $( ".person.entity" ).css("margin", "0");
            $( ".person.entity" ).css("padding", "0");
            $( ".person.entity span.category" ).css("display", "none");
        }
    });

    $('#person').change(function() {
        if (this.checked) {
            $( ".city.entity" ).css("background", "#7aecec");
            $( ".city.entity" ).css("margin", "0 0.25em");
            $( ".city.entity" ).css("padding", "0.45em 0.6em");
            $( ".city.entity span.category" ).css("display", "inline");
        } else {
            $( ".city.entity" ).css("background", "none");
            $( ".city.entity" ).css("margin", "0");
            $( ".city.entity" ).css("padding", "0");
            $( ".city.entity span.category" ).css("display", "none");
        }
    });


});