#!/usr/bin/env python3
"""
Copyright 2021 CEA LIST

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import flask
import jmespath
import logging
import os
import requests
import sys
import time
import traceback
import urllib.parse

from datetime import datetime
from flask import (Blueprint, request, jsonify, make_response, abort)
from marshmallow import Schema, fields, validate

from pkm_python_client.pkm_client import PKMClient
from pkm_python_client.pkm_client import Log

from .decoder_ner_tool_service import NER

startTimestamp = datetime.now()

PKM_API_HOST = "pkm"

PKM_USER = os.environ.get('PKM_USER')
PKM_PASSWORD = os.environ.get('PKM_PASSWORD')
PKM_CERT = os.environ.get('PKM_CERT')
if PKM_CERT is None:
    PKM_CERT = "/ssl/pkm_docker.crt"

debug = os.environ.get('DEBUG', default="true").lower() == "true"

api = Blueprint('api_v1', __name__)


class PKMNERSchema(Schema):
    project_id = fields.Str(required=True)  # pkm project to query
    # pkm path to query to retrieve json data containing the text to analyze
    path = fields.Str(required=True)
    # json path allowing to retrieve the text to analyze in the retrieved data
    access = fields.Str(required=False)
    lang = fields.Str(required=False)  # the text language
    model = fields.Str(required=False)  # the NE model to use
    invocationID = fields.Str(required=False)

class PKMNERResultSchema(Schema):
    message = fields.Str(required=True)
    status = fields.Boolean(required=True)


class Text(Schema):
    text = fields.Str()
    lang = fields.Str(required=False)  # the text language
    model = fields.Str(required=False)  # the NE model to use


class NERChunk(Schema):
    text = fields.Str(required=True)
    type = fields.Str(required=True)
    pos = fields.Integer(required=True)
    len = fields.Integer(required=True)


class NERResult(Schema):
    entities = fields.List(fields.Nested(NERChunk()),
                           required=True)


def send_result(message, status, pkm=None, project_id=None, invocation_id=None,
                resultsToAdd=[]):
    assert type(resultsToAdd) == list, (f"resultsToAdd ({resultsToAdd}) should be a "
                                        f"list but it is a {type(resultsToAdd)}")
    app = flask.current_app
    app.logger.error(message)
    res = {
      "message": message,
      "status": (status < 300)}
    result_schema = PKMNERResultSchema()
    loaded = result_schema.load(res)

    logging.info(f"Returning {result_schema.dumps(loaded)}.")
    if invocation_id is not None and pkm is not None:
        # Put in invocationResults the paths of artifacts put in the pkm with type of
        # handled data ?
        pkm.update_invocation(
            project_id, invocation_id, 0 if status < 300 else status,
            resultsToAdd)
    return result_schema.dumps(loaded), status


@api.route('/pkmner', methods=['POST'])
def pkmner(invocationID=None):
    """ Named Entities Recognition in text stored in the PKM
    ---
    post:
      parameters:
      - name: key
        description: Access key to the PKM (optional, depending on server
                      configuration)
        in: header
        schema:
          type: string
      - name: invocationID
        in: path
        description: decoder invocation id
        required: false
        schema:
          type: string
      - in: data
        schema: PKMNERSchema
      responses:
        200:
          description: NER completed.
          content:
            application/json:
              schema: PKMNERResultSchema
        400:
          description: Bad Request.
          content:
            application/json:
              schema: PKMNERResultSchema
        500:
          description: An unexpected error has occurred.
          content:
            application/json:
              schema: PKMNERResultSchema
    """
    try:
        app = flask.current_app

        key = None
        if 'keyParam' in request.headers:
            key = request.headers.get('keyParam')
        elif 'key' in request.headers:
            key = request.headers.get('key')

        form_data = request.get_json()
        project_id = None
        if "project_id" in form_data:
            project_id = form_data['project_id']
        invocation_id = request.args.get('invocationID', None)
        if invocation_id is None and 'invocationID' in form_data:
            invocation_id = form_data['invocationID']
        print(f"pkmner project_id: {project_id}, invocation_id: {invocation_id}",
              file=sys.stderr)

        pkm = PKMClient(cacert=PKM_CERT, key=key)
        log = Log(pkm=pkm, project=project_id, tool="ner_tool",
                  invocation_id=invocation_id)

        if key is None and PKM_USER and PKM_PASSWORD:
            if debug:
                flask.current_app.logger.debug("user's login")
            status = pkm.login(user_name=PKM_USER, user_password=PKM_PASSWORD)
            if not status:
                message = f"user's login to pkm failed: {status}"
                flask.current_app.logger.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)
        elif key is None:
            errorString = (
                "Either pkm key should be passed as http header 'key' or"
                " user/password env vars should be defined")
            flask.current_app.logger.error(errorString)
            return send_result(errorString, 400, pkm, project_id, invocation_id)

        ner_schema = PKMNERSchema()
        errors = ner_schema.validate(form_data)
        if errors:
            message = (f"/pkmner Failed to validate request "
                       f"data '{form_data}' with respect to the schema. "
                       f"Error: {errors}.")
            log.error(message)
            app.logger.error(message)
            return send_result(message, 400, pkm, project_id, invocation_id)

        path = urllib.parse.unquote(form_data['path'])  # pkm path to query to

        # retrieve json data containing the text to analyze
        # access: json path allowing to retrieve the text to analyze in the
        # retrieved data
        if 'access' in form_data:
            access = form_data['access']
        else:
            access = None

        # model is be used to select models specialy trained for a given use case
        if 'model' in form_data:
            model = form_data['model']
        else:
            model = "Decoder"
        if 'lang' in form_data:
            lang = form_data['lang']
        else:
            lang = "eng"

        if debug:
            message = f"getting {path}"
            app.logger.debug(message)
            log.message(message)
        status, doc = pkm.call(method="GET", path=path)
        if status != 0:
            message = f"Getting {path} failed."
            app.logger.error(message)
            log.error(message)
            return send_result(message, 500, pkm, project_id, invocation_id)

        if debug:
            message = f"retrieved doc is {doc}"
            app.logger.debug(message)
            log.message(message)
        if access is None:
            texts = doc
            if not isinstance(texts, list):
                texts = [texts]
        else:
            access = urllib.parse.unquote(access)
            if debug:
                message = f"json path is: {access}"
                app.logger.debug(message)
                log.message(message)
            try:
                texts = jmespath.search(access, doc)
            except jmespath.exceptions.EmptyExpressionError as e:
                message = f"JMESpath search failed on empty access {access}: {e}"
                app.logger.debug(message)
                log.message(message)
                return send_result(message, 400, pkm, project_id, invocation_id)
            except Exception as e:
                message = f"JMESpath search failed on {access}: {e}"
                app.logger.debug(message)
                log.message(message)
                return send_result(message, 400, pkm, project_id, invocation_id)
            if texts is None:
                texts = []
            if debug:
                message = f"selected texts are: {texts}"
                app.logger.debug(message)
                log.message(message)

        results = []
        for text in texts:

            message = f"Analyzing {text}."
            logging.info(message)
            log.message(message)
            try:
                status, res = NER.ner(text, lang=lang, model=model)
            except ValueError as e:
                message = (f"Got a ValueError from ner in pkmner: {e}. "
                           f"Stackstrace: \n{traceback.format_exc()}")
                app.logger.error(message)
                log.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)

            if status:
                message = f"NER completed {status}, {res}."
                app.logger.info(message)
                log.message(message)
                results.append(res)
            else:
                message = f"NER failed with {status}: {res}."
                app.logger.info(message)
                log.message(message)

        result = {}
        result['path'] = path
        result['access'] = access
        result['ner'] = results

        # TODO implement writing the srl at the right location in the annotations.
        if debug:
            message = (f"writing back ner result of project {project_id} "
                       f"and path {urllib.parse.quote(path, safe='')}")
            app.logger.debug(message)
            log.message(message)
            message = f"result to write is: {result}"
            app.logger.debug(message)
            log.message(message)
        status, res = pkm.call(method="PUT",
                               path=f"annotations/{project_id}",
                               payload=[result])
        if status != 0:
            message = (f"Writing back ner result of project {project_id} "
                       f"and path {path} failed")
            app.logger.error(message)
            log.error(message)
            return send_result(message, 500, pkm, project_id, invocation_id)

        return send_result("Success", 200, pkm, project_id, invocation_id,
                           [dict(path=(
                               f"/annotations/{project_id}"
                               f"?path={urllib.parse.quote(path, safe='')}"
                               f"&access={urllib.parse.quote(access, safe='')}"),
                            type="annotations")])
    except Exception as e:
        message = (f"Got an unexpected exception in pkmner: {e}. "
                   f"Stackstrace: \n{traceback.format_exc()}")
        app.logger.error(message)
        log.error(message)
        return send_result(message, 500, pkm=None, project_id=None, invocation_id=None)


@api.route('/ner', methods=['POST'])
def ner():
    """Extract Named Entities from text
    ---
    post:
      parameters:
      - in: data
        schema: Text
      responses:
        200:
          schema: NERResult
        400:
          description: Bad Request.
          content:
            application/json:
              schema: NERResult
        500:
          description: An unexpected error has occurred.
          content:
            application/json:
              schema: NERResult
    """

    app = flask.current_app
    text_schema = Text()
    form_data = request.get_json()
    # This below validates the JSON data existence and data type
    errors = text_schema.validate(form_data)
    if errors:
        return {
          "message": "Missing or sending incorrect data to analyze text."
            }, 400
    else:

        text = form_data['text']

        # model is be used to select models specialy trained for a given use case
        if 'model' in form_data:
            model = form_data['model']
        else:
            model = "Decoder"
        if 'lang' in form_data:
            lang = form_data['lang']
        else:
            lang = "eng"

        logging.info(f"Analyzing {text}, {lang}, {model}.")
        status, res = NER.ner(text, lang="eng", model=model)

        if status:
            logging.info(f"Analysis completed {status}, {res}.")
        else:
            message = f"Anaylsis failed {status}."
            app.logger.info(message)
            logging.error(message)
        result_schema = NERResult()

        loaded = result_schema.load(res)

        logging.info(f"Returning {result_schema.dumps(loaded)}.")
        return result_schema.dumps(loaded), 200


