#!/usr/bin/env python3
"""
Copyright 2021 CEA LIST

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import requests
import logging
import sys


class NER(object):

    @staticmethod
    def ner(text: str = None, lang: str = "eng", model: str = "Decoder"):
        '''
        Do named entities recognition on given text.
        Parameters
        ----------
        text : str
            The text to analyze
        lang : str
            The language of the text
        model : str
            The model to use to analyze; the pipeline in Lima words
        '''
        if not isinstance(text, str):
            print(f"NER.ner Error. text must be a string {text}", file=sys.stderr)
            raise ValueError(f"NER.ner Error. text must be a string {text}")

        url = f'http://lima:8080/?lang={lang}&pipeline={model}'
        logging.info(f"NER.ner {url}: {text}")
        response = requests.post(url, data=text)
        if response.status_code == requests.codes.ok:
            json_response = response.json()
            entities = []
            for token in json_response["tokens"]:
                logging.info(f"Handling `{token}'.")
                if token and token.startswith("T"):
                    # T1\tIdentifier 0 8\twhichptr'
                    try:
                        id, entity, entity_text = token.split("\t")
                    except ValueError as e:
                        logging.error(f"Error splitting token in 3 on tabs: {token}, "
                                      f"{e}")
                        continue
                    try:
                        type, pos, len = entity.split(" ")
                    except ValueError as e:
                        logging.error(f"Error splitting entity in 3 on spaces: "
                                      f"id: {id}, entity: {entity}, "
                                      f"entity_text: {entity_text}; {e}")
                        continue
                    try:
                        entities.append({"text": entity_text, "type": type,
                                        "pos": int(pos), "len": int(len)})
                    except ValueError as e:
                        logging.error(f"Error converting data to int: {type}, {pos}, "
                                      f"{len}, {e}")
                        continue

            result = {"entities": entities, "text": text}
            return True, result
        else:
            return False, response.reason
