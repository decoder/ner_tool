#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

DECODER_HOME=/home/gael/Projets/Decoder-ICT-16


rm  -f ${DECODER_HOME}/PackRun/resources/SpecificEntities/*.bin

pushd ${DECODER_HOME}/WP4/clima
./update.sh
popd

